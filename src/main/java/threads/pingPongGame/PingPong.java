package threads.pingPongGame;

public class PingPong {
    public static void main(String[] args) {
        Table table = new Table();
        Ping ping=new Ping(table);
        Pong pong=new Pong(table);
        ping.start();
        pong.start();
    }
}
