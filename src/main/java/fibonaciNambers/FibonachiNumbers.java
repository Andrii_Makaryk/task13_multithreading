package fibonaciNambers;

public class FibonachiNumbers implements Runnable{
    private int number;
    public int answer;

    public FibonachiNumbers(int number) {
        this.number = number;
    }

    public void run() {
        if(number<=2){
            answer=1;
        }else{
            try{
                FibonachiNumbers fibonaciOne=new FibonachiNumbers(number-1);
                FibonachiNumbers fibonaciTwo=new FibonachiNumbers(number-2);
                Thread threadOne=new Thread(fibonaciOne);
                Thread threadTwo=new Thread(fibonaciTwo);
                threadOne.start();
                threadTwo.start();
                threadOne.join();
                threadTwo.join();
                answer=fibonaciOne.answer+fibonaciTwo.answer;
            }catch(InterruptedException ex){
                ex.printStackTrace();
            }
        }

    }
}
